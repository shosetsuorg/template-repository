# Icons

Store icons that extensions & authors use here.

This helps prevent malicious actors from changing remote resources.