#!/bin/bash -x

echo "Downloading lua documentation"
rm _doc.lua
wget https://gitlab.com/shosetsuorg/kotlin-lib/-/raw/main/_doc.lua

#echo "Download javascript documentation"
#wget https://gitlab.com/shosetsuorg/kotlin-lib/-/raw/main/doc.js

# TODO Download extension tester

echo "Downloading index.schema.json"
rm index.schema.json
wget https://gitlab.com/shosetsuorg/kotlin-lib/-/raw/main/index.schema.json
