# Extensions

This directory contains extensions as referenced by `index.json`.

Each extension must be placed in a sub-directory as specified by the language tag.

Example: An Extension marked as "eng" must be placed under `eng/`.